var noticeBoar = {
  attachNoticeBoar: function() {
    var max = 7;
    var min = 0;

    var noticeSchedules = document.querySelectorAll("div#main div#primary_container div.pbr_dashboard div.column div.portlet_section.notice_schedule");
    [].forEach.call(noticeSchedules, function(el) {
      var header = el.querySelector("div.portlet_section_header h2");
      if (header !== null && header.textContent.indexOf("Notice Board") > -1) {
        header.textContent = "Notice Boar";

        var picNumber = Math.floor(Math.random() * (max - min + 1)) + min;
        var boarUrl = chrome.extension.getURL("/images/boars/notice_boar_" + picNumber + ".png");
        var boarLocations = el.querySelectorAll("div.portlet_section_content");
        
        [].forEach.call(boarLocations, function(location) {
          location.insertAdjacentHTML("afterbegin", '<object id="notice-boar" data="" type="image/png" style="width: 100%; padding-bottom: 10px;">' +
    '<img src="' + boarUrl + '" style="width: 100%; padding-bottom: 10px;" /></object>');
        });

        var boarPhrases = ["boar", "baby boar", "wild boar", "wild boar attack leopard", "boar drawing", "monkey riding boar"];
        var boarIndex = Math.floor(Math.random() * boarPhrases.length);
        var boarPhrase = boarPhrases[boarIndex];

        $.ajax({
          dataType: "json",
          url: 'https://www.googleapis.com/customsearch/v1',
          crossDomain: true,
          data: {
            q: boarPhrase,
            searchType: "image",
            cx: "000955162771466362623:d93f4irkspu",
            key: "AIzaSyAnB0q1_GvrkRotWdpFsPc8sURvGUJxHTQ",
            safe: "high"
          },
          success: function ( json ) {
            if (json.items.length > 0) {
              var rand = Math.floor(Math.random() * json.items.length),
                results = json.items;
              //var url = results[rand].url.replace("http://","https://");
              var url = results[rand].link;
              $('object#notice-boar').attr('data', url);
            }
          }
        });
      }
    });
  },
  attachTerminatorToElement: function(el, selector) {
    var minPicIndex = 0;
    var maxPicIndex = 2;
    var workOrderDescription = el.querySelector(selector);
    if (workOrderDescription != null && workOrderDescription.textContent.indexOf("Termination") > -1) {
      var picNumber = Math.floor(Math.random() * (maxPicIndex - minPicIndex + 1)) + minPicIndex;
      var localPicUrl = chrome.extension.getURL("/images/terminators/terminator_" + picNumber + ".png");
      var picInsertLocation = el.querySelectorAll(selector);

      var terminatorIconUrl = chrome.extension.getURL("/images/terminators/terminator_icon.png");
      [].forEach.call(picInsertLocation, function(location) {
        location.insertAdjacentHTML("beforeend", "<img style='position: relative; height: 32px; width: 32px; float: right;' src='" + terminatorIconUrl + "' />");
      });
    }
  },
  attachInvoluntaryTerminator: function() {
    var noticeSchedules = document.querySelectorAll("div#main div#primary_container div#work_manager_tabs table.data_table.dataTable tr");
    var self = this;
    [].forEach.call(noticeSchedules, function(el) {
      self.attachTerminatorToElement(el, "td:nth-child(9)");
      self.attachTerminatorToElement(el, "td:nth-child(8)");
    });
  }
};

noticeBoar.attachNoticeBoar();
noticeBoar.attachInvoluntaryTerminator();